// The main function

$(document).ready(function () {
  const url = "https://billgatesbooks.wl.r.appspot.com/";

  $("#cc").on("click", function (e) {
    var cc = "2020";
    var myUrl = url + cc;
    var textContent = "Book List Year: 2020!";

    $("#urlArea").val(myUrl);
    $("#textArea").val(textContent);

    $("ul li").each(function () {
      $(this).remove();
    });

    $.ajax({
      type: "GET",
      dataType: "json",
      url: myUrl,
      success: function (data) {
        var books = data["2020"];
        $.each(books, function () {
          $("ul").append(
            '<li><image src="' +
              this["image"] +
              '">' +
              "<h1><b>Title: </b>" +
              this["title"] +
              "</h1><br><b>Author: </b>" +
              this["author"] +
              "<br><b>Year: </b>" +
              this["year"] +
              "<br>" +
              "<b>Abstract: </b>" +
              this["abstract"] +
              "<hr></hr>" +
              "</li>"
          );
        });
        $("ul li").attr("class", "items");
      },
      error: function (e) {
        console.log("Error!");
        console.log(e);
      }
    });
  });
  $("#bb").on("click", function (e) {
    var bb = "2019";
    var myUrl = url + bb;
    var textContent = "Book List Year: 2019!";

    $("#urlArea").val(myUrl);
    $("#textArea").val(textContent);

    $("ul li").each(function () {
      $(this).remove();
    });

    $.ajax({
      type: "GET",
      dataType: "json",
      url: myUrl,
      success: function (data) {
        var books = data["2019"];
        $.each(books, function () {
          $("ul").append(
            '<li><image src="' +
              this["image"] +
              '">' +
              "<h1><b>Title: </b>" +
              this["title"] +
              "</h1><br><b>Author: </b>" +
              this["author"] +
              "<br><b>Year: </b>" +
              this["year"] +
              "<br>" +
              "<b>Abstract: </b>" +
              this["abstract"] +
              "<hr></hr>" +
              "</li>"
          );
        });
        $("ul li").attr("class", "items");
      },
      error: function (e) {
        console.log("Error!");
        console.log(e);
      }
    });
  });
  $("#aa").on("click", function (e) {
    var aa = "2018";
    var myUrl = url + aa;
    var textContent = "Book List Year: 2018!";

    $("#urlArea").val(myUrl);
    $("#textArea").val(textContent);

    $("ul li").each(function () {
      $(this).remove();
    });

    $.ajax({
      type: "GET",
      dataType: "json",
      url: myUrl,
      success: function (data) {
        var books = data["2018"];
        $.each(books, function () {
          $("ul").append(
            '<li><image src="' +
              this["image"] +
              '">' +
              "<h1><b>Title: </b>" +
              this["title"] +
              "</h1><br><b>Author: </b>" +
              this["author"] +
              "<br><b>Year: </b>" +
              this["year"] +
              "<br>" +
              "<b>Abstract: </b>" +
              this["abstract"] +
              "<hr></hr>" +
              "</li>"
          );
        });
        $("ul li").attr("class", "items");
      }
    });
  });
});